import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthProvider {

  constructor(
    private fireAuth: AngularFireAuth
  ) {
  }

  public authState() {
    return this.fireAuth.authState;
  }

  public authorize() {
    return this.fireAuth.auth;
  }

}
