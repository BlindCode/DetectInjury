import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InActionPage } from './in-action';

@NgModule({
  declarations: [
    InActionPage,
  ],
  imports: [
    IonicPageModule.forChild(InActionPage),
  ],
})
export class InActionPageModule {}
