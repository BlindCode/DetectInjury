import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import Timer from 'time-counter';
import { AngularFireDatabase } from "angularfire2/database";
import { Vibration } from '@ionic-native/vibration';
import { AuthProvider } from '../../providers/auth';

interface Person {
  name: string;
  danger: string;
  tracking: boolean;
}

@IonicPage()
@Component({
  selector: 'page-in-action',
  templateUrl: 'in-action.html',
})
export class InActionPage {
  timer: string = '0:00';
  friends: Array<any> = [];
  person: Person;
  countUpTimer;
  danger: string;

  constructor(
    private auth: AuthProvider,
    private nav: NavController,
    private db: AngularFireDatabase,
    private viewCtrl: ViewController,
    private vibration: Vibration
  ) {
    this.countUpTimer = new Timer();
    this.countUpTimer.start();
    this.countUpTimer.on('change', time => this.timer = time);
  }

  stop() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    this.auth.authState().subscribe(val => {
      this.listFriends(val.uid);
      this.mightBeDangerVibrate(val.uid);
    });
  }

  getWeightByDanger(danger) {
    if (danger === 'yes')
      return 1;
    if (danger === 'maybe')
      return 2;
    if (danger === 'no')
      return 3;
    return 4;
  }

  listFriends(myId) {
    this.db.object(`/users/${myId}`)
      .valueChanges()
      .subscribe((person: Person) => {
        this.person = person;
        this.db.list('/users')
          .valueChanges()
          .subscribe(friends => {
            console.log(friends);
            this.friends = friends
              .filter((f: any) => f.name !== this.person.name);

            this.friends = friends.map(friend => ({
              ...friend,
              sortIndex: this.getWeightByDanger(friend['danger'])
            }));

            this.friends.sort((a, b) => a.sortIndex - b.sortIndex);

            this.friends.forEach(friend => {
              for (let key in friend) {
                if (key === 'danger' && friend[key] === 'yes') {
                  this.friendindanger();
                }
              }
            })
          });
      });
  }

  friendindanger() {
    this.vibration.vibrate([500, 100, 500, 100, 500, 100, 500]);
  }

  iamindanger() {
    this.auth.authState().subscribe(val => {
      this.db.object('users/' + val.uid).update({ danger: 'yes' });
    });
  }

  checkmark() {
    this.auth.authState().subscribe(val => {
      this.db.object('users/' + val.uid).update({ danger: 'no' });
    });
  }

  mightBeDangerVibrate(uid) {
    this.db.object('users/' + uid).valueChanges().subscribe((val: any) => {
      val.danger === 'maybe'
        ? this.vibration.vibrate([1000, 500, 1000])
        : this.vibration.vibrate(0);
    });
  }
}
